const paymentMethodsEnum = {
    creditCard: 'credit_card',
    boleto: 'boleto',
};

const productsTablesEnum = {
    class: 'Classes',
    course: 'Courses',
};

module.exports = {
    paymentMethodsEnum,
    productsTablesEnum,
};
