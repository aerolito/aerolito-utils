/* eslint-disable no-restricted-syntax */
const dotProp = require('dot-prop-immutable');

module.exports = (obj) => {
    let payload = {
        ...obj,
    };
    const identifyAndRemove = (o, subpath) => {
        if (Array.isArray(o)) {
            if (o.length === 0) {
                if (subpath) payload = dotProp.delete(payload, subpath);
                else return o.map((item, oKey) => identifyAndRemove(item, subpath ? `${subpath}.${oKey}` : `${oKey}`));
            }
        } else if (o !== null && typeof o === 'object') {
            Object.keys(o).forEach((oKey) => {
                const oValue = o[oKey];
                return identifyAndRemove(oValue, subpath ? `${subpath}.${oKey}` : `${oKey}`);
            });
        } else if (o === undefined || o === 'undefined' || o === null || o === '' || subpath.includes('__typename') || subpath.includes('Symbol(id)')) {
            if (subpath) {
                payload = dotProp.delete(payload, subpath);
            }
        }
        return payload;
    };

    identifyAndRemove(payload);
    return payload;
};


module.exports.clearUndefinedValues = (object) => {
    function loop(obj) {
        const t = obj;
        for (const v in t) {
            if (typeof t[v] === 'object') loop(t[v]);
            else if (t[v] === undefined) delete t[v];
        }
        return t;
    }
    return loop(object);
};
