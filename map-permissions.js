const app = {
    oli: 'oli',
    aerolito: 'arl',
};

const services = {
    radar: 'rdr',
    class: 'cls',
    select: 'slct',
    feed: 'feed',
    courses: 'crs',
};

const types = {
    data: 'data',
    loop: 'loop',
    session: 'sssn',
    speaker: 'spkr',
    video: 'vdeo',
    aggregator: 'aggt',
    course: 'crs',
    class: 'cls',
};

const operations = {
    query: 'qry',
    mutate: 'mtt',
    mutateOther: 'mtto',
    mutateVisibility: 'mttv',
    moderate: 'mttm',
};

module.exports.mapAtributtesPermissions = (permissions, organization = 'aerolito') => permissions && permissions.length > 0 && permissions.map((p) => ({
    Name: `custom:${app[p.app]}_${services[p.service]}_${types[p.type]}`,
    Value: `${organization}:${p.operations.map((op) => `${operations[op]}`).join('|')},`,
}));
