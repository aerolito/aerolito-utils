const { flatMap } = require('lodash');

module.exports = (user) => {
    const recm = user.recommendations;
    const pref = user.preferences;
    const perm = user.permissions;
    return {
        ...user,
        recommendations: (recm && Object.keys(recm) && Object.keys(recm).length > 0
        && flatMap(Object.keys(recm).map((org) => recm[org] && recm[org].length > 0
            && recm[org].map((rec) => ({
                ...rec,
                organization: org,
            }))))) || {},
        preferences: (pref && Object.keys(pref) && Object.keys(pref).length > 0
        && flatMap(Object.keys(pref).map((org) => pref[org] && Object.keys(pref[org])
            && Object.keys(pref[org]).length > 0
            && Object.keys(pref[org]).map((p) => ({
                organization: org,
                field: p,
                value: pref[org][p],
            }))))) || {},
        permissions: (perm && Object.keys(perm) && Object.keys(perm).length > 0
        && flatMap(Object.keys(perm).map((org) => ({
            permissions: perm[org],
            organization: org,
        })))) || {},
    };
};
