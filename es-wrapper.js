const elasticsearch = require('elasticsearch');
const httpAwsEs = require('http-aws-es');

module.exports = (hosts, testMode) => {
    const esParams = {
        hosts,
    };
    // Because we use ordinary elasticsearch container instead of AWS elasticsearch for integration tests
    if (!testMode) esParams.connectionClass = httpAwsEs;

    const es = new elasticsearch.Client(esParams);
    return {
        index: ({
            index,
            type,
            id,
            body,
            refresh,
        }) => new Promise((resolve, reject) => {
            es.index({
                index,
                type,
                id,
                body,
                refresh,
                timeout: '5m',
            }, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        }),
        remove: ({
            index,
            type,
            id,
            refresh,
        }) => new Promise((resolve, reject) => {
            es.delete({
                index,
                type,
                id,
                refresh,
            }, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        }),
        exists: ({
            index,
            type,
            id,
            refresh,
        }) => new Promise((resolve, reject) => {
            es.exists({
                index,
                type,
                id,
                refresh,
            }, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        }),
        search: ({
            index,
            body,
            size,
        }) => new Promise((resolve, reject) => {
            es.search({
                index,
                body,
                size,
            }, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        }),
        get: ({
            index,
            type,
            id,
            refresh,
        }) => new Promise((resolve, reject) => {
            es.get({
                index,
                type,
                id,
                refresh,
            }, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        }),
        mget: ({
            body,
        }) => new Promise((resolve, reject) => {
            es.mget({
                body,
            }, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        }),
        msearch: ({
            body,
        }) => new Promise((resolve, reject) => {
            es.msearch({
                body,
            }, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        }),
        bulk: ({
            body,
            refresh,
        }) => new Promise((resolve, reject) => {
            es.bulk({
                body,
                refresh,
            }, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        }),
        indicesDelete: (index = '_all') => new Promise((resolve, reject) => {
            es.indices.delete({
                index,
            }, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        }),
    };
};
